function(ADD_INTERNAL_MODULE)
    set(options)
    set(oneValueArgs NAME)
    set(multiValueArgs DEPENDS)
    cmake_parse_arguments(AIM
        "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    ExternalProject_Add(${AIM_NAME}
        SOURCE_DIR "${CMAKE_SOURCE_DIR}/${AIM_NAME}"
        INSTALL_DIR "${CMAKE_BINARY_DIR}/${AIM_NAME}"
        DEPENDS ${AIM_DEPENDS})
endfunction(ADD_INTERNAL_MODULE)