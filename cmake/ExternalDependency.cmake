include(ExternalProject)

set(DEPENDENCIES
  CACHE INTERNAL "The required dependencies of the pipeline" FORCE
)

set(IN_SITU_PIPELINE_HAS_UNMENT_DEPENDENCIES FALSE
  CACHE INTERNAL "Will be set to true if there are unmet dependencies" FORCE
)

function(ADD_DEPENDENCY_ENTRY)
  set(options REQUIRED)
  set(oneValueArgs NAME MESSAGE FOUND)
  set(multiValueArgs EXTERNAL_PROJECT_ADD_ARGUMENTS)
  cmake_parse_arguments(ADE
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  set(DEPENDENCIES ${DEPENDENCIES} ${ADE_NAME}
    CACHE INTERNAL "The required dependencies of the pipeline" FORCE
  )
  set(${ADE_NAME}_FOUND ${ADE_FOUND}
    CACHE INTERNAL "Indicates whether ${ADE_NAME} was found on the system" FORCE
  )
  set(${ADE_NAME}_REQUIRED ${ADE_REQUIRED}
    CACHE INTERNAL "Indicates whether ${ADE_NAME} is required and cannot be build from source" FORCE
  )
  set(${ADE_NAME}_MESSAGE ${ADE_MESSAGE}
    CACHE INTERNAL "Details about the ${ADE_NAME} dependency" FORCE
  )
  if (ADE_REQUIRED)
    set(IN_SITU_PIPELINE_HAS_UNMENT_DEPENDENCIES TRUE
      CACHE INTERNAL "Will be set to true if there are unmet dependencies" FORCE
    )
  endif () 
endfunction(ADD_DEPENDENCY_ENTRY)

function(ADD_EXTERNAL_DEPENDENCY)
  set(options)
  set(oneValueArgs NAME)
  set(multiValueArgs EXTERNAL_PROJECT_ADD_ARGUMENTS)
  cmake_parse_arguments(AED
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  find_package(${AED_NAME} QUIET)

  if (${AED_NAME}_FOUND)
    # Create a dummy target to avoid dependency errors
    add_custom_target(build_${AED_NAME})
    
    add_dependency_entry(
      NAME ${AED_NAME}
      FOUND TRUE
      MESSAGE "${${AED_NAME}_CONFIG}"
    )
  else ()
    message(STATUS "${AED_NAME} was not found on the system, it will be build from source")
    ExternalProject_Add(build_${AED_NAME} ${AED_EXTERNAL_PROJECT_ADD_ARGUMENTS})
    
    add_dependency_entry(
      NAME ${AED_NAME}
      FOUND FALSE
      MESSAGE "will be build from source"
    )
  endif ()
    
endfunction(ADD_EXTERNAL_DEPENDENCY)

function(DISPLAY_DEPENDENCY_SUMMARY)
  message(STATUS "Dependencies:")
  foreach (DEPENDENCY ${DEPENDENCIES})
    if (${DEPENDENCY}_FOUND)
      set(SYMBOL "X")
    elseif (${DEPENDENCY}_REQUIRED)
      set(SYMBOL " ")
    else ()
      set(SYMBOL "B")
    endif ()
    message(STATUS "  [${SYMBOL}] ${DEPENDENCY}: ${${DEPENDENCY}_MESSAGE}")
  endforeach ()
endfunction(DISPLAY_DEPENDENCY_SUMMARY)

function(CHECK_FOR_MISSING_DEPENDENCIES)
  if (IN_SITU_PIPELINE_HAS_UNMENT_DEPENDENCIES)
    message(SEND_ERROR "Some required dependencies could not be found, see above!")
  endif ()
endfunction(CHECK_FOR_MISSING_DEPENDENCIES)

