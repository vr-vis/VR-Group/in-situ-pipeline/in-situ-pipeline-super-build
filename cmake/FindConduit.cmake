find_file(
  Conduit_CONFIG
  "conduit.cmake"
  HINTS
    ${Conduit_DIR}
    ${CONDUIT_DIR}/lib/cmake  # For backwards comability
  PATH_SUFFIXES
    lib/cmake
)

find_path(
  Conduit_INCLUDE_DIRECTORIES
  "conduit/conduit.h"
  HINTS
    ${Conduit_DIR}/../../include
    ${CONDUIT_DIR}/include  # For backwards comability
)

mark_as_advanced(
  Conduit_CONFIG
  Conduit_INCLUDE_DIRECTORIES
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Conduit
  FOUND_VAR Conduit_FOUND
  REQUIRED_VARS
    Conduit_CONFIG
    Conduit_INCLUDE_DIRECTORIES
)

set (Conduit_CONFIG ${Conduit_CONFIG} PARENT_SCOPE)

if (Conduit_FOUND AND NOT TARGET conduit)
  include(${Conduit_CONFIG})
  set_target_properties(conduit
    PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES ${Conduit_INCLUDE_DIRECTORIES}
  )
endif ()
