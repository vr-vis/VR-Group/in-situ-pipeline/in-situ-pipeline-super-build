# In Situ Pipeline
This repository contains a meta build to build all components of the in situ pipeline and its dependencies. It uses cmake to build the following libraries:
* [Contra](https://devhub.vr.rwth-aachen.de/VR-Group/contra)
* [Nesci](https://devhub.vr.rwth-aachen.de/VR-Group/nesci)
* [Cone](https://devhub.vr.rwth-aachen.de/VR-Group/cone)
* [NEST streaming module](https://devhub.vr.rwth-aachen.de/VR-Group/nest-streaming-module)

# Options
Here are all options and their default values to change the configuration.

* `BUILD_NEST=OFF`: This option can be turned on to compile the nest simulator if you don't have it installed already. (Note: the in situ pipeline requires a special build of the simulator that includes [nestio](https://github.com/jougs/nest-simulator/tree/nestio). If you already have it installed, you can specify it using `-Dwith-nest=<path-to-nest-config>`.
* `WITH_SHARED_MEMORY=ON`: Include the shared memory transport protocol
* `WITH_ZEROMQ=ON`: Include the ZeroMQ transport protocol